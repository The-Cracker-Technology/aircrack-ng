NOCONFIGURE=1 ./autogen.sh

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Autogen.sh... PASS!"
else
  # houston we have a problem
  exit 1
fi

./configure --with-experimental=yes

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Configure... PASS!"
else
  # houston we have a problem
  exit 1
fi

make uninstall

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Uninstall previous versions... PASS!"
else
  # houston we have a problem
  exit 1
fi

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

make install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make install... PASS!"
else
  # houston we have a problem
  exit 1
fi
